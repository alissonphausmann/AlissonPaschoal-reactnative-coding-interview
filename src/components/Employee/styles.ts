import { StyleSheet } from "react-native";

export default StyleSheet.create({
  employeeItem: {
    padding: 10,
    paddingVertical: 15,
    flex: 1,
  },
  employeeBorder: {
    borderWidth: 1,
  },
  employeeAvatar: {
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  employeeInfo: {
    justifyContent: "space-around",
  },
});
