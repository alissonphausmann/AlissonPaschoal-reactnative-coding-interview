import React from "react";

import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
import { EmployeeDetailScreen, EmployeesScreen } from "../screens";
import { ScreenParamsList } from "./paramsList";

const mockedEmployee = {
  email: "ukerluke@yahoo.com",
  firstname: "Rosina",
  lastname: "O'Hara",
  phone: "+6310545745178",
  website: "http://ondricka.net",
};

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator<ScreenParamsList>();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: "#6C3ECD",
        headerTitleAlign: "center",
      }}
    >
      <Stack.Screen component={EmployeesScreen} name="Employees" />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
};

export function MainNavigator() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={StackNavigator} />
      <Drawer.Screen
        name="UserProfile"
        component={EmployeeDetailScreen}
        initialParams={{ employee: mockedEmployee }}
      />
    </Drawer.Navigator>
  );
}
